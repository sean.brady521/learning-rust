pub fn run () {
  println!("Hello from the print.rs file");
  println!("{} is from {}", "Brad", "Mass");

  // Positional Arguments
  println!("{0} is from {1} and {0} likes to {2}", "brad", "mass", "code");

  // Names Arguments
  println!("{name} lieks to play {activity}", name = "John", activity = "activity");

}